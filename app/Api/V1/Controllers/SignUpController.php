<?php

namespace App\Api\V1\Controllers;

use Config;
use Auth;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SignUpController extends Controller
{
    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth)
    {

        $credentials = $request->only(['email', 'password']);

            $user = new User($request->all());
            if (!$user->save()) {
                throw new HttpException(500);
            }
            try {
            $token = Auth::guard()->attempt($credentials);

            if(!$token) {
                //throw new AccessDeniedHttpException();
                return response()->json(['status'=>'notok','message'=>'Please Enter Valid Credentials']);
            }

        } catch (JWTException $e) {
            throw new HttpException(500);
        }
            $user=User::where('email',$request['email'])->get();
            if (!Config::get('boilerplate.sign_up.release_token')) {
                return response()->json([
                    'status' => 'ok',
                    'token' => $token,
                    'user' => $user
                ], 201);
            }

            $token = $JWTAuth->fromUser($user);
            return response()->json([
                'status' => 'ok',
                'token' => $token,
                'user' => $user
            ], 201);
        }

}


