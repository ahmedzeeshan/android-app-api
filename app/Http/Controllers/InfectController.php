<?php

namespace App\Http\Controllers;

use App\ingredients;
use App\User;
use App\UserPreferences;
use thiagoalessio\TesseractOCR\TesseractOCR;
use Auth;
use Illuminate\Http\Request;



class InfectController extends Controller
{

    public function UserProfile(){
        $user_id = Auth::user()->id;
        $user = User::where('user_id',$user_id)->get();

        return response()->json(['success'=>true,'user'=>$user],200);
    }

    public function EditProfile(Request $request){

        if ($request->has('email')&&$request->has('name')
            &&$request->has('password'))
        {
            $users_id=Auth::user()->id;
            $file = $request->file('image');

            $name = "/uploads/".time() . '.' . $file->getClientOriginalExtension();

            $request->file('image')->move("uploads", $name);

                $user=User::find($users_id);
                $user->name=$request['name'];
                $user->password=$request['password'];
                $user->image=$name;
                $user->email=$request['email'];
                $user->save();

                return response()->json(['success'=>true,'message'=> 'Profile Updated successfully'],200);



        }
        else {
            return response()->json(['success' => false, 'message' => 'Unable To Update Profile'], 200);
        }




    }

    public function IsSignUp(Request $request){


        if ($request->has('diet_plan_id')){


            $user_id = Auth::user()->id;

            User::where('id',$user_id)->update(['diet_plan_id'=>$request->diet_plan_id,'is_signup_complete'=>true]);
            $user=User::where('id',$user_id)->get();

            return response()->json(['success'=>true,'user'=>$user],200);
        }
        else {
            return response()->json(['success' => false, 'message' => 'Invalid '], 200);

        }
    }

}